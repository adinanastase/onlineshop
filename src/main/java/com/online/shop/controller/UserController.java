package com.online.shop.controller;

import com.online.shop.dto.UserDto;
import com.online.shop.service.UserService;
import com.online.shop.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @GetMapping("/addUser")
    public String addUserPageGet(Model model,
                                 @RequestParam(value = "userAddedSuccessfully", required = false) Boolean userAddedSuccessfully) {
        System.out.println(userAddedSuccessfully);
        UserDto userDto = new UserDto();
        model.addAttribute("userDto", userDto);
        if (userAddedSuccessfully != null && userAddedSuccessfully) {
            model.addAttribute("message", "Congrats! You have successfully added a new user!");
        }
        return "addUser";
    }

    @PostMapping("/addUser")
    public String addUserPagePost(@ModelAttribute UserDto userDto, BindingResult bindingResult,
                                  RedirectAttributes redirectAttributes) {
        userValidator.validate(userDto, bindingResult);
        if (bindingResult.hasErrors()) {
            return "addUser";
        }
        userService.addUser(userDto);
        redirectAttributes.addAttribute("userAddedSuccessfully", true);
        return "redirect:/addUser";
    }
}
